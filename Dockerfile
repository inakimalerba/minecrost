FROM debian:unstable

RUN apt-get update && \
    apt-get install -y wget eatmydata

RUN apt-get update && \
    eatmydata apt-get install -y openjdk-17-jre

WORKDIR /data
ENV VERSION='1.20.2-48.0.37'
RUN wget https://maven.minecraftforge.net/net/minecraftforge/forge/${VERSION}/forge-${VERSION}-installer.jar

RUN echo "eula=true" > eula.txt
RUN java -jar forge-${VERSION}-installer.jar --installServer

COPY server.properties server.properties

VOLUME /data/mods
VOLUME /data/config
VOLUME /data/world

EXPOSE 25565

ENTRYPOINT ./run.sh
