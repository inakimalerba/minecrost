# Minecrost server

All the files to run the server.

> Current version: 1.12.2

## Using the mods for your client.

* Install the [Shignima Launhcer](https://teamshiginima.com/update/)
* Run with:
```bash
$ java -jar ShiginimaSE_v4400/linux_osx/Shiginima\ Launcher\ SE.v4400.jar
```
Login as some user and select the minecraft version 1.12.2 (edit profile-> use version) and hit play.
When the game is installed close it.

* Install the [Forge Stuff](https://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html)
Download the latest for the 1.12.2.
```bash
$ java -jar forge-1.12.2-14.23.5.2847-universal.jar
```
Install the client into the .minecraft directory (if not changed).

* Clone this repo somewhere.
* Create a symlink to use the mods from here.
```bash
$ ln -s ${WHEREVER_YOU_CLONE_REPOS}/minecraft-server/data/mods ~/.minecraft/mods
```
* Run Shiginima and select the patched version (release 1.12.2-forge.1.12.2**)

Also there is mods client only that are optional (mods_client_only/). You can link or copy them
into mods.

## How to run the server.

```bash
docker-compose up
```

## How to add stuff.

The data persists on data/{config,mods,world}.
